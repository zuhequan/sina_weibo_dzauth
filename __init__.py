#coding:utf-8
"""
把sina微博统一登录加解密算法dzauth从php翻译为python
函数：dzauth(string, XAUTH_TK_DATA_ENCRIPT_KEY, operation='encode')
参数：
      string：待加密或解密的文本
      XAUTH_TK_DATA_ENCRIPT_KEY：加解密的密钥
      operation：encode表示加密，decode表示解密
"""
import time
import hashlib
import base64
import time
import math

def microtime(get_as_float = False) :
    if get_as_float:
        return time.time()
    else:
        return '%f %d' % math.modf(time.time())

def dzauth(string, XAUTH_TK_DATA_ENCRIPT_KEY, operation='encode'):
    APP_LOCAL_TIMESTAMP = int(time.time())
    def md5(txt):
        return hashlib.md5(str(txt)).hexdigest()

    def strlen(txt):
        return len(txt)

    def base64_decode(txt):
        return txt.decode('base64')

    def base64_encode(txt):
        return base64.encodestring(txt)

    def substr(txt, start, length=0):
        if length > 0:
            return txt[start:length]
        else:
            return txt[start:]

    operation = operation.upper()
    expiry		= 0;
    ckey_length = 4;
    key	= XAUTH_TK_DATA_ENCRIPT_KEY;
    keya	= md5(key[0:16]);
    keyb	= md5(key[16:]);
    if operation == 'DECODE':
        _tmp_str = string[0:ckey_length]
    else:
        _tmp_str = md5(microtime())[-ckey_length:]
    if ckey_length:
        keyc = _tmp_str
    else:
        keyc = ""
    cryptkey = keya + md5(keya + keyc);
    key_length = strlen(cryptkey);
    if operation == 'DECODE':
        string = base64_decode(substr(string, ckey_length))
    else:
        if expiry:
            _tmp_ = expiry + APP_LOCAL_TIMESTAMP
        else:
            _tmp_ = 0
        _tmp_ = str(_tmp_).rjust(10, '0')
        string = _tmp_ + substr(md5(string + keyb), 0, 16) + string
    string_length = strlen(string);

    result = '';
    box = range(0, 256);

    rndkey = [];
    for i in xrange(0, 256):
        rndkey.append(ord(cryptkey[i % key_length]))
    j = 0
    for i in xrange(0, 256):
        j = (j + box[i] + rndkey[i]) % 256;
        tmp = box[i];
        box[i] = box[j];
        box[j] = tmp;

    a = 0
    j = 0
    i = 0
    for i in range(0, string_length):
        a = (a + 1) % 256;
        j = (j + box[a]) % 256;
        tmp = box[a];
        box[a] = box[j];
        box[j] = tmp;
        result = result + chr(ord(string[i]) ^ (box[(box[a] + box[j]) % 256]));


    if operation == 'DECODE':
        if (int(substr(result, 0, 10)) == 0 or int(substr(result, 0, 10)) - APP_LOCAL_TIMESTAMP > 0) and  substr(result, 10, 16) == substr(md5(substr(result, 26) + keyb), 0, 6):
            return substr(result, 26);
        else:
            return ''
    else:
        return keyc + base64_encode(result)


if __name__ == "__main__":
    a = dzauth('09990909000', "XAUTH_TK_DATA_ENCRIPT_KEY",'encode')
    print len(a),a
    print dzauth(a, "XAUTH_TK_DATA_ENCRIPT_KEY",'decode')